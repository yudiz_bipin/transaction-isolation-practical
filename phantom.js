const { sequelize, Transaction } = require('./db')
const { sales } = require('./sales')
require('./db')

// phantom read

// isolation level = serialize

async function phantomRead() {
    const t1 = await sequelize.transaction({
        isolationLevel: Transaction.ISOLATION_LEVELS.READ_COMMITTED
    })
    const t2 = await sequelize.transaction({
        isolationLevel: Transaction.ISOLATION_LEVELS.READ_COMMITTED

    })
    try {
        console.log('........Phantom Read Issue...........')

        console.log('Transaction 1 Started...')
        const sum1 = await sales.sum('nQuantity', { transaction: t1 })
        console.log(`sum of nQuantity from t1 : ${sum1}`)

        console.log('Transaction 2 Started...')
        await sales.create({ sProductName: "pencil", nQuantity: 20, nPrice: 5 }, { transaction: t2 })
        console.log('New row added in Transaction 2')

        console.log('Transaction 2 COMMIT')
        await t2.commit()

        console.log('Transaction 1 continues...')
        const sum2 = await sales.sum('nQuantity', { transaction: t1 })
        console.log(`sum of nQuantity from t1 : ${sum2}`)

        console.log('Transaction 1 COMMIT')
        await t1.commit()

    } catch (error) {
        await t1.rollback()
        await t2.rollback()
    }
}


//phantom read with isolation level = serializable

async function phantomReadWithIsolation() {
    const t1 = await sequelize.transaction({
        isolationLevel: Transaction.ISOLATION_LEVELS.SERIALIZABLE
    })
    const t2 = await sequelize.transaction({
        isolationLevel: Transaction.ISOLATION_LEVELS.READ_COMMITTED

    })
    try {
        console.log('........Phantom Read Solution : SERIALIZABLE...........')

        console.log('Transaction 1 Started...')
        const sum1 = await sales.sum('nQuantity', { transaction: t1 })
        console.log(`sum of nQuantity from t1 : ${sum1}`)

        console.log('Transaction 2 Started...')
        console.log('Transaction 2 gets blocked...')
        await sales.create({ sProductName: "pencil", nQuantity: 20, nPrice: 5 }, { transaction: t2 })
        console.log('New row added in Transaction 2')

        console.log('Transaction 2 COMMIT')
        await t2.commit()

        console.log('Transaction 1 continues...')
        const sum2 = await sales.sum('nQuantity', { transaction: t1 })
        console.log(`sum of nQuantity from t1 : ${sum2}`)

        console.log('Transaction 1 COMMIT')
        await t1.commit()
    } catch (error) {
        await t1.rollback()
        await t2.rollback()
    }

}


// phantomRead()
phantomReadWithIsolation()


//sample data
// (async()=>{
//     await sales.sync({force:true})

//     await sales.bulkCreate([
//     {sProductName:"book",nQuantity:10,nPrice:20},
//     {sProductName:"pen",nQuantity:5,nPrice:10}
// ])
// })()