const { Sequelize, DataTypes, Transaction } = require('sequelize')

const sequelize = new Sequelize('ACID', 'root', 'Yudiz@321', {
    host: "localhost",
    dialect: "mysql",
    define: {
        freezeTableName: false
    },
    logging: false
});

(async () => {
    try {
        await sequelize.authenticate()
        console.log('db connected')

    } catch (err) {
        console.log('error in db connection', err)
    }
})()

module.exports = { sequelize, DataTypes, Transaction }
