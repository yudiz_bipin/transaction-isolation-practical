const { sequelize, Transaction } = require('./db')
const { sales } = require('./sales')
require('./db')

//non-repeatable reads 

// isolation level  = Repeatable read

async function nonRepeatableRead() {

    const t1 = await sequelize.transaction({
        isolationLevel: Transaction.ISOLATION_LEVELS.READ_COMMITTED
    })
    const t2 = await sequelize.transaction()

    try {
        console.log('........Non-Repeatable Read Issue...........')

        console.log('Transaction 1 Started...')
        const sum1 = await sales.sum('nQuantity', { transaction: t1 })
        console.log(`sum of nQuantity from t1 : ${sum1}`)

        console.log('Transaction 2 Started...')
        await sales.increment('nQuantity', { by: 5, where: { id: 1 }, transaction: t2 })
        console.log('nQuantity updated in Transaction 2')

        console.log('Transaction 2 COMMIT')
        await t2.commit()

        console.log('Transaction 1 continues...')
        const sum2 = await sales.sum('nQuantity', { transaction: t1 })
        console.log(`sum of nQuantity from t1 : ${sum2}`)

        console.log('Transaction 1 COMMIT')
        await t1.commit()
    }
    catch (error) {
        await t1.rollback()
        await t2.rollback()
    }

}

//non repeatable read solution with isolation level = repeatable read

async function nonRepeatableReadWithIsolation() {
    const t1 = await sequelize.transaction({
        isolationLevel: Transaction.ISOLATION_LEVELS.REPEATABLE_READ
    })
    const t2 = await sequelize.transaction()
    try {
        console.log('........Non-Repeatable Read Solution : REPEATABLE_READ...........')

        console.log('Transaction 1 Started...')
        const sum1 = await sales.sum('nQuantity', { transaction: t1 })
        console.log(`sum of nQuantity from t1 : ${sum1}`)

        console.log('Transaction 2 Started...')
        await sales.increment('nQuantity', { by: 5, where: { id: 1 }, transaction: t2 })
        console.log('nQuantity updated in Transaction 2')

        console.log('Transaction 2 COMMIT')
        await t2.commit()

        console.log('Transaction 1 continues...')
        const sum2 = await sales.sum('nQuantity', { transaction: t1 })
        console.log(`sum of nQuantity from t1 : ${sum2}`)

        console.log('Transaction 1 COMMIT')
        await t1.commit()
    }
    catch (error) {
        await t1.rollback()
        await t2.rollback()
    }

}


    // nonRepeatableRead()
    // nonRepeatableReadWithIsolation()



//sample data
// (async()=>{
//     await sales.sync({force:true})

//     await sales.bulkCreate([
//     {sProductName:"book",nQuantity:10,nPrice:20},
//     {sProductName:"pen",nQuantity:5,nPrice:10}
// ])
// })()