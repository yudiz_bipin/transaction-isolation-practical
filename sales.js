const { sequelize, DataTypes } = require('./db')

const sales = sequelize.define('sales', {
    sProductName: DataTypes.STRING,
    nQuantity: DataTypes.INTEGER,
    nPrice: DataTypes.INTEGER
})

module.exports = { sales }