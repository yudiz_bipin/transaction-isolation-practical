const { sequelize, Transaction } = require('./db')
const { sales } = require('./sales')
require('./db')


//dirty read
// isolation level = read uncommited

//solution
//isolation level = read commited

async function dirtyRead() {

    const t1 = await sequelize.transaction({
        isolationLevel: Transaction.ISOLATION_LEVELS.READ_UNCOMMITTED,
    })
    const t2 = await sequelize.transaction({
        isolationLevel: Transaction.ISOLATION_LEVELS.READ_UNCOMMITTED,
    })

    try {
        await sales.sync()
        console.log('........DirtyRead Issue...........')
        console.log('Transaction 1 Started...')

        //read 1
        const data = await sales.findAll({ transaction: t1 })
        console.log(`Query ::  NAME | QTY | PRICE`)
        data.forEach(d => console.log(`Query ::  ${d.dataValues.sProductName} | ${d.dataValues.nQuantity} | ${d.dataValues.nPrice}`))

        //update mQuantity
        console.log('Transaction 2 Started...')
        await sales.increment("nQuantity", { by: 5, where: { id: 1 }, transaction: t2 })
        console.log('nQuantity Updated by 5 from Transaction 2')

        console.log('Transaction 1 Continues...')
        const sum = await sales.sum('nQuantity', { transaction: t1 })
        console.log(`nQuantity Sum : ${sum}`)

        console.log('Transaction 1 COMMIT')
        await t1.commit()

        console.log('Transaction 2 ROLLBACK')
        await t2.rollback()

    } catch (error) {

        t2.rollback
        t1.rollback
        console.log('error in transaction', error)
        reject(false)
    }

}

// dirty read with isolation level of READ Commited
async function dirtyReadWithRC() {
    const t1 = await sequelize.transaction({
        isolationLevel: Transaction.ISOLATION_LEVELS.READ_COMMITTED,
    })
    const t2 = await sequelize.transaction({
        isolationLevel: Transaction.ISOLATION_LEVELS.READ_COMMITTED,
    })

    try {
        await sales.sync()
        console.log('........DirtyRead Solution : READ_COMMITED...........')
        console.log('Transaction 1 Started...')

        //read 1
        const data = await sales.findAll({ transaction: t1 })
        console.log(`Query ::  NAME | QTY | PRICE`)
        data.forEach(d => console.log(`Query ::  ${d.dataValues.sProductName} | ${d.dataValues.nQuantity} | ${d.dataValues.nPrice}`))

        //update mQuantity
        console.log('Transaction 2 Started...')
        await sales.increment("nQuantity", { by: 5, where: { id: 1 }, transaction: t2 })
        console.log('nQuantity Updated from Transaction 2')

        console.log('Transaction 1 Continues...')
        const sum = await sales.sum('nQuantity', { transaction: t1 })
        console.log(`nQuantity Sum : ${sum}`)

        console.log('Transaction 1 COMMIT')
        await t1.commit()

        console.log('Transaction 2 ROLLBACK')
        await t2.rollback()
    } catch (error) {
        t2.rollback
        t1.rollback
        console.log('error in transaction', error)
        reject(false)
    }

}


// dirtyRead()
dirtyReadWithRC();



//sample data
// (async () => {
//     await sales.sync({ force: true })
//     await sales.bulkCreate([{ sProductName: "book", nQuantity: 10, nPrice: 20 }, { sProductName: "pen", nQuantity: 5, nPrice: 10 }])
// })()